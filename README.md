# cd_lib: NCBI DevOps Platform 2 CD

This is NCBI's tool for managing the continuous deployment of projects. It uses [`ytt`][ytt] to render application definitions to an environment repository managed by [Argo CD][argo]

[argo]: https://argoproj.github.io/argo-cd/
[ytt]: https://carvel.dev/ytt

More documentation about `cd_lib` is available on this [Confluence](https://confluence.ncbi.nlm.nih.gov/display/DO/POC+for+ClinicalTrials.gov+%28CTG%29+in+DevOps+Platform2.0) page.
## How to use `cd_lib`

`cd_lib` is not intended to be used directly.  Instead, developers are expected to use a `CI` template in order to build, test, and deploy software.  Various `CI` templates are consumers of the `cd_lib` project.

In order to use `cd_lib`, please create a new development project using an "Instance" template.

**:exclamation: Please do not put any secrets into your code!**  Instead refer to the [Secrets Handling] section below - we have convenient functionality to allow you to pass secrets to your deployed application!

If you have complex requirements for deploying your application, you can customize your CD pipelines by following the instructions in the section below, [Customizing your CD Pipelines].

By default, `cd_lib` creates simple notifications when deployments succeed and fail, read more about those notifications [here](#pipeline-feedback)

## Contributing

Please read the [contribution guidelines](CONTRIBUTING.md).


## Background

GitLab attempts to co-locate code and CI/CD procedures in a developer-friendly way.  This is accomplished through the use of a `.gitlab-ci.yml` file.  If a project (code repository) contains a `.gitlab-ci.yml` file, GitLab will attempt to process it and perform tasks described.  For a more comprehensive overview of GitLab's Continuous Integration please refer to [GitLab documentation](https://docs.gitlab.com/ee/ci/quick_start/README.html)

The `cd_lib` project is an implementation of CI and CD intended to build Docker images and deploy them to OpenShift clusters in compliance with NCBI security policies while ensuring that deployments are 100% codified ([GitOps](https://www.weave.works/technologies/gitops/)).

There are many other implementations of CI/CD through GitLab's `.gitlab-ci.yml`, the most significant of which is GitLab's own [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).  At this time, Auto DevOps does not work on OpenShift.


## CI/CD process

> **:memo: Note:** This section was written before the creation of separate CI templates, and
> is therefore somewhat out of date. It needs to be updated.

`cd_lib` runs through four stages, `build`, `build_images`, `test`, and `deploy`.
 * You can add jobs to any stage, as well as create your own stages before, between, and after existing stages.

#### The `build` stage
The build stage is intended to be the "final" stage of CI "build" jobs.  One could customize the CI/CD pipeline such that there are stages (and jobs within those stages) which run prior to the `build` stage.  The current CI implementation includes a single job in the `build` stage: [generate_build_configurations](https://gltest11.be-md.ncbi.nlm.nih.gov/prd/do/v2/prototype/cd_lib/-/blob/docs/Build.gitlab-ci.yml#L6-24) from a separate, "CI", repository.  In the context of the project being built, the [ci](https://gltest11.be-md.ncbi.nlm.nih.gov/prd/do/v2/prototype/ci) repository is cloned and a single script is run.  This script scans the root and top-level folders of the project for the presence of Dockerfile(s).  It then creates "children" GitLab CI jobs within the CI pipeline to build (and store in the GitLab Registry) 0 or more Docker images, as well as scan those images for security issues.
  * Artifacts can be passed to individual "docker build" jobs for respective service - refer to "custom.yml" below.

#### The `build_images` stage
The `build_images` stage is used to run child GitLab CI jobs (generated in `build` stage prior) to build and store Docker images.

Due to security limitations, we are unable to build Docker containers using the Docker daemon.  To work around this limitation we use an unprivileged container building software called [makisu](https://github.com/uber/makisu).  For the most part, `makisu build` is a drop-in replacement for any `docker build` command, although there are differences and limitations.
The `build_images` stage runs in the context of your project, meaning this [command](https://gltest11.be-md.ncbi.nlm.nih.gov/prd/do/v2/prototype/cd_lib/-/blob/master/Build.gitlab-ci.yml#L5-35) runs as if it was included in your projects `.gitlab-ci.yml` file directly.


If the `build_images` stage is successful, 0 or more Docker images should be built and stored in your projects' container registry.

#### The `test` stage
The `test` stage is intended to test your built application.  The `test` stage runs if the `build_images` stage passed successfully.  A number of scanners from GitLab's Auto-DevOps are included by default [here](https://gltest11.be-md.ncbi.nlm.nih.gov/prd/do/v2/prototype/cd_lib/-/blob/master/Build.gitlab-ci.yml#L38-44).
It is possible for you to create any number of jobs to test your code, your built image, or anything else you might desire to test.
Like the `build` and `build_images` stages, the `test` stage runs mostly in the context of your project.
One can test any aspect of CI during the `test` stage using jobs defined in `custom.yml` (see below).  One can test actual code, artifacts of prior jobs, built containers, etc.
A `checkmarx` scanner job runs for `production` and `develop` branch deployments.

#### The `deploy` stage
The `deploy` stage runs if the `test` stage runs successfully.  The `deploy` stage runs almost entirely in the context of `cd_lib`.  This [trigger](https://gltest11.be-md.ncbi.nlm.nih.gov/prd/do/v2/prototype/cd_lib/-/blob/master/Simple.gitlab-ci.yml#L31-34) is equivalent of your project's pipeline running the pipeline of `cd_lib` with a number of parameters.
What happens next is described in scripts [here](https://gltest11.be-md.ncbi.nlm.nih.gov/prd/do/v2/prototype/cd_lib/-/blob/master/.gitlab-ci.yml#L62-69).  A very short description is as follows:

* Part 1:
  - `cd_lib` checks to make sure you are allowed to deploy the project you are trying to deploy.
  - `cd_lib` creates valid Kubernetes YAML declarations in order to run your Docker image(s), including any secrets you want to pass to your application.
  - `cd_lib` checks this YAML into a repository called `env` (also hosted on GitLab).  At this point `cd_lib` is effectively done.

* Part 2:

  A tool called [Argo CD](https://argoproj.github.io/argo-cd/) "watches" the `env` repository and attempts to manifest the declared YAML within any number of OpenShift (Kubernetes) cluster(s).  It's UI can be reached here: https://10.10.99.11/applications

  The `env` repository is our source of truth.


## Secrets handling
Secrets are implemented as follows: as a Maintainer of your project you can go into Settings -> CI/CD -> Variables and create a number of variables.  There are two types of Variables, `File` and `Variable`.

#### The `Variable` type
Consider a variable of type `Variable`, Key: `VAR1` and Value: `VALUE1`.  When your Docker container starts in OpenShift, an environmental variable `VAR1` would be set to `VALUE1` before the application inside your container starts.

#### The `File` type
Consider a variable of type `File`, Key: `FILE1` and Value: `CONTENT1`.  When your Docker container starts in OpenShift, `cat /etc/FILE1/value` would print `CONTENT1`.  This file would be available before the application inside your container starts.

#### Environments
There exists basic logic to send the right variable VALUE to the right branch.  When creating a variable, note the free-text box called "Environment scope" - this is where you can enter the name of your branch (create a new string if it doesn't exist).  Therefore, you can have two variables of Key: PASSWORD with different values, scoped to different branches (Ignore "Make variables available to the running application by prepending the variable key with K8S_SECRET_" - this is Auto DevOps functionality which we do not use):
![](images/var2.png)

The logic is rather simple:
 - the `production` branch will always use Variables and Files where Environment is "production".
 - the `develop` branch will always use Variables and Files where Environment is "develop".
 - any other branch will use Variables and Files where Environment is "develop", UNLESS there exists at least one Variable or File where Environment matches the name of the branch being deployed.  If at least one such Variable or File exists, only those Variables and Files of the matching Environment will be created.

 * Never put secrets into variables prefixed with `YTT_`!  More information on this is available in the YAML matching section below.


## Pipeline Feedback

Pipelines using `cd_lib` for builds and deployments provide feedback on their success or failure in a number of ways.

* **Email**: When pipelines in Gitlab complete, regardless of their success or failure, they will send an email to the person who triggered the run (either manually or through a commit). This email will have a link to the pipeline and state if it had run successfuly or not. Because `cd_lib` generates downstream pipelines to run in different contexts (see [deployments](#the-deploy-stage)), a separate email will be sent for each of these downstream pipelines.

* **Slack**: Currently, `cd_lib` sends a message to [#notify-do-cd-p2](https://ncbi.slack.com/archives/C01JNH3HMD4) on the NCBI Slack when a deployment either succeeds or fails with a note about the success or failure of the deployment and a reference to the git revision hash it was trying to deploy.

* **Status Badges**: Some developers may be interested in including status badges in their project to have an at-a-glance view of the health of the application. There are currently two relevant badges available:

  - Gitlab pipelines badge: Available at `https://gltest.be-md-ncbi.nlm.nih.gov/<path/to/project>/badges/<branch>/pipeline.svg`, this badge shows the state of the last pipeline to be run on the specified branch.
  - ArgoCD deployments badge: Available at `https://<cluster-argo-host>/api/badge?name=<path-to-project-slug>-<branch-slug>`, this badge shows the health and state of the deployment managed by ArgoCD for a particular branch.

  You can include these in your project by adding them to your `README.md` file, or by using the [Gitlab project badge](https://docs.gitlab.com/ee/user/project/badges.html) feature.

If you want to change the timeouts for the process of waiting for a deployment to succeed after it's been committed to the `env` repository, add  the following
-  to the top of your `custom.yml` file:

## Miscellaneous notes
 - Each branch of each project exists in its' own Kubernetes namespace.
 - You can view the status of your deployment in Argo CD. (`https://10.10.99.11/applications/<namespace>`)
 - If you want to exec into a Pod of your deployment, use the OpenShift console or CLI (Guide from SYS [here](https://confluence.ncbi.nlm.nih.gov/display/SYS/OpenShift+4+User+Guide))
 - Istio is used to route traffic.  You can generally reach your deployed services at `http://<service>.<namespace>.okdtest.p2.ncbi.nlm.nih.gov:80` from outside the cluster, and at `http://<service>-stable.<namespace>.svc.cluster.local:5000` from inside the cluster.
 - Istio Grafana is available here: https://grafana-istio-system.apps.okdtest.be-md.ncbi.nlm.nih.gov/
 - Kubernetes Grafana is available here: https://grafana-openshift-monitoring.apps.okdtest.be-md.ncbi.nlm.nih.gov/
 - Kiali Console is available here: https://kiali-istio-system.apps.okdtest.be-md.ncbi.nlm.nih.gov/
 - Jaeger is available here: https://jaeger-istio-system.apps.okdtest.be-md.ncbi.nlm.nih.gov/

## Known Issues
> **:memo: Note:** make  sure these are updated to reflect the current status of the project.

 - The first deployment of any branch can take up to 5 minutes to be visible on the network.  This is due to lag in Istio routing updates.
 - We need to come up with a way to encrypt secrets for the destination cluster (such that the RSA keypair isn't shared between clusters)
 - Docker registry tokens expire after 30 days, meaning if a deployment in OpenShift is older than 30 days and crashes, OpenShift might not be able to pull the container from the registy to restart the deployment.
 - There is potential for, and evidence of, downtime during deployment, likely related to bugs in Istio.  These bugs have reportedly been fixed as of Istio version 1.8, however, our OpenShift clusters are using Service Mesh 2.0 by Red Hat, which is based on Istio version 1.6 which contains the bugs.


## Customizing your CD Pipelines

You can create any number of parallel jobs for any stage of the CI/CD process.  The most common use case is custom tests to run during the `test` stage, however, you can also create jobs for any other stage, as well as your own stages.  These jobs would run under the context of your project.

To customize the CI/CD process, please make your `.gitlab-ci.yml` looks like so (adding a `custom.yml` include):
``` 
include:
  - project: 'prd/do/v2/prototype/cd_lib'
    ref: master
    file: 'Simple.gitlab-ci.yml'
  - 'custom.yml'
```
Then, include your valid GitLab CI YAML into a new file in the root of your repository called `custom.yml`.  Please(!) follow this standard so that we can accommodate planned changes for how `cd_lib` is implemented.

* If you want to change the timeouts for the process of waiting for a deployment to succeed after it's been committed to the `env` repository, add  the following
  to the top of your `custom.yml` file:

  ```yml
  variables:
    DEPLOYMENT_TIMEOUT: <desired timeout in seconds>
  ```

## Multi-container deployments
It is possible to deploy multiple containers (services) from the same repository, at the same time.  The current CI process assumes the following:
if a "Dockerfile" exists in the root folder of the repository, a `root` service is considered to be defined.
if a "Dockerfile" exists in a top-level folder of the repository (example: `./foo/Dockerfile`), a service with the name of the top-level folder is considered to be defined (service `foo` in this case).
 * limitations: service names should ONLY be lowercase alphanumeric characters

For each "service", a "docker build" job will be created.  For example, in the [kotlin](https://gltest11.be-md.ncbi.nlm.nih.gov/prd/do/v2/prototype/project-templates/kotlin) template project, the Dockerfile for the `root` service is built using a job called `build-root-image`.  If `./foo/Dockerfile` existed, the job to build that image would be called `build-foo-image`.

Each container, by default, is deployed using a simplistic Argo Rollout.  The container is assumed to listen on port 5000 TCP and respond to HTTP health checks with a 200 status code (`GET /`).  It is possible to completely customize any part of the Rollout, including removing it completely and deploying the container using something else entirely.

#### Passing artifacts to the "docker build image" job of a service.
It is possible to pass artifacts to `build-root-image` and `build-foo-image` "docker build" jobs listed above. 
An example is available under the `kotlin` project template (refer to `custom.yml`).  The project includes a custom CI job with the following definition:
```
build-root-package:
  stage: build
  image: openjdk:13-buster
  script:
    - ./gradlew shadowJar
  artifacts:
    when: always
    paths:
      - $CI_PROJECT_DIR/build/libs/web.jar
```
Effectively, if `custom.yml` has a job named `build-<service>-package` and the same named "service" container exists (meaning `./<service>/Dockerfile` also exists), the `build-<service>-image` job will include an artifact dependency from the `build-<service>-package` job of the same CI pipeline.  This allows a developer to use an existing Docker container to perform a quick build `./gradlew shadowJar` and pass the built jar file to packaging (rather than building the application "inside" a Dockerfile).


## Patching deployment YAML
It is possible to patch the YAML of the Argo Rollout for any given service.
To patch the "root" service, you would create a file called "Rollout.patch" in the root of the project
To patch the "foo" service, you would create `./foo/Rollout.patch`.

Patching is performed using a tool called YTT, as a final step in the YAML hydration process.  The YTT tool website provides a playground which can be used to preview patching (here is an [example](https://carvel.dev/ytt/#gist:https://gist.github.com/cppforlife/865b820033685c49a5a477ed8c210579))

An example of a patch is available to view for the SRA Data Locator application:
```
spec:
  replicas: $YTT_REPLICAS
  template:
    spec:
      containers:
      #@overlay/match by=overlay.subset({'name': 'main'}), expects="0+"
      - name: main
        #@overlay/replace
        readinessProbe:
          failureThreshold: 3
          httpGet:
            path: /4/retrieve/?&acc=SRR000001
            port: 5000
            scheme: HTTP
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 1
        #@overlay/replace
        livenessProbe:
          failureThreshold: 30
          httpGet:
            path: /?pos:0=2&pos:1=healthcheck
            port: 5000
            scheme: HTTP
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 1
```
In the patch above, the number of Pods is modified (default in the template is 1), and the liveness and readiness Kubernetes probes are modified as well.

#### Variable expansion in YAML patching functionality
Note in the example above, `replicas: ` is replaced with a variable $YTT_REPLICAS.  This functionality allows a developer to "patch" a deployment with "environment-specific" values.  For example, one could run 10 Pods for the "production" branch deployment, and 2 in all the other deployments. 

This is dangerous functionality, because we are mixing the store for secrets with the store of variables which will end up in plaintext YAML.  To provide a naive protection against the use of actual secrets in YAML templates, only variables which begin with a `YTT_` will be replaced, meaning if you want to parametrize `Rollout.patch`, you can only use variables such as `YTT_REPLICAS`, `YTT_CPU`, `YTT_MEMORY`, and so on.  

>***===:exclamation: VERY IMPORTANT :exclamation:===***
>
>You MAY NOT put any sensitive values into varibles which begin with `YTT_` - the values will not be stored in an encrypted manner!
>


## `ci_image`
This Docker [image](https://gltest11.be-md.ncbi.nlm.nih.gov/prd/do/v2/prototype/ci_image) is used to perform the build (using `makisu`) as well as the YAML generating and saving tasks using a variety of tools.
It is available in the OpenShift cluster from an ImageStream, at image-registry.openshift-image-registry.svc:5000/prd-do-v2-prototype-ci-image-master/ci-image:latest
